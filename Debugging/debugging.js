//The following code is split into 3 programs. All of them have errors within them, and you must use the Google Chrome developer tools to trace through the lines of code and debug them to fix the errors.
//To use the Chrome Developer tools, pres f12 on your keyboard.


//To check the code, uncomment the corresponding function.
task1()
task2()
task3()

//How can I get this printing the numbers 1 to 10 to the output area?
//There are 3 errors
function task1(){
    // outputArea2 had a spelling mistake - it is case sensitive
    let outputArea = document.getElementById("outputArea1")
    // loop from 1 to 11, the last number is not printed
    for(let i = 1; i < 11; i++){
        // We have to add to the innerHTML, not replace it every time
        outputArea.innerHTML += i + "<br>"                  
    }                        
}                         

     
//I want to print the words "Icecream is tasty" "Icecream is sweet" "Icecream is the best" to the output area, but it doesn't work!
//There are 3 errors
function task2(){
    let outputArea = document.getElementById("outputArea2") 
    let myArray = ["is tasty", "is sweet", "is the best"]        
    // let myFood = Icecream
    let myFood = myArray
    // myarray = myArray
    for(let i = 0; i < myFood.length; i++){
        outputArea.innerHTML += "Icecream " + myFood[i] + "<br>"          
    }                     
} 

//How can I print out the properties of the object into the output area?
//There are 2 errors
function task3(){   
    let outputArea = document.getElementById("outputArea3")
    let myObj = {
        // morgan should be a string
        name: "Morgan",
        age: 50,
        likesPizza: true
    }

    for (let prop in myObj){
        outputArea.innerHTML += myObj[prop] + "<br>"
    }                            
} 

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    const myArr = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19,
        -51, -17, -25]
        
    //Question 1 here 
    let posOdd = [];
    let negEven = [];
    let excess = []

    myArr.filter(elem => {
        if(elem>0 && elem%2 !==0){
            posOdd.push(elem)
        }else if(elem<0 && elem%2==0){
            negEven.push(elem)
        }else{
            excess.push(elem)
        }
    })
    
    output=`Positive Odd: ${posOdd.toString()}\nNegative Even: ${negEven.toString()}`

    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function rollDie(){
    return Math.floor((Math.random() * 6) + 1)
}

function question2(){
    let output = "";
    let one = 0;
    let two = 0;
    let three = 0;
    let four = 0;
    let five = 0;
    let six = 0;

    for(i=0; i<60_000; i++){
        const currRoll = rollDie()
        if(currRoll === 1){
            one++
        }else if(currRoll === 2){
            two++
        }else if(currRoll === 3){
            three++
        }else if(currRoll === 4){
            four++
        }else if(currRoll === 5){
            five++
        }else if(currRoll === 6){
            six++
        }else{
            console.log("ERROR")
        }
    }
    //Question 2 here
    output = `Frequency of die rolls\n 1: ${one}\n 2: ${two}\n 3: ${three}\n 4: ${four}\n 5: ${five}\n 6: ${six}`
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}


function question3(){
    let output = "Frequency of die rolls\n";
    let freqArr = [0, 0, 0, 0, 0, 0, 0]
    
    //Question 3 here 
    for(let i=0; i<60_000; i++){
        const currRoll = rollDie()
        freqArr[currRoll]++
    }
    for (let i in freqArr){
        if(i>0){
            output += `${i}: ${freqArr[i]}\n`;
        }
    }
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}


function question4(){
    let output = "Frequency of dice rolls\n";
    let dieRolls = {
        frequencies: {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0
        },
        Total: 60_000,
        Exceptions: ""
    }
    for(let i=0; i<60_000; i++){
        dieRolls.frequencies[rollDie()]++
    }
    const onePerc = (1/100) * (dieRolls.Total/6);
    
    for(let i in dieRolls.frequencies){
        if(dieRolls.frequencies[i] >= (onePerc + dieRolls.Total/6)){
            dieRolls.Exceptions += `${i.toString()}, `
        }else if(dieRolls.frequencies[i] <= (dieRolls.Total/6 - onePerc)){
            dieRolls.Exceptions += `${i.toString()} `
        }
    }
    output += `Total Rolls: ${dieRolls.Total}\nFrequencies:\n`
    for(let i in dieRolls.frequencies){
        output += `${i}: ${dieRolls.frequencies[i]}\n`
    }
    output += `Exceptions: ${dieRolls.Exceptions}`
    
    //Question 4 here 
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}



function taxOwed(income){
    if(income <=18_200){
        return 0

    }else if(income >=18_201 && income <=37_000){
        return (income - 18_200) * 0.19

    }else if(income>= 37_001 && income <=90_000){
        return 3572 + ((income - 37_000) * 0.325)

    }else if(income >=90_001 && income <=180_000){
        return 20_797 + ((income - 90_000) * 0.37)

    }else if(income >=180_000){
        return 54_097 + ((income - 180_000) * 0.45)

    }else{
        return 0
    }
}


class Person{
    constructor(name, income){
        this.name = name;
        this.income = income
    }
}


function question5(){
    let output = "";
    // let person = {
    //     name: "Jane",
    //     income: 127050
    // }
    const person = new Person("Jane", 127_050)
    output = `${person.name}'s income is: $${person.income}, and his/her tax owed is: $${taxOwed(person.income)}`    
    //Question 5 here 
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}


//to check the code, uncomment the corresponding question.
question1();
question2();
question3();
question4();
question5();